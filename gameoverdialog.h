#ifndef GAMEOVERDIALOG_H
#define GAMEOVERDIALOG_H

#include <QDialog>

namespace Ui {
class GameOverDialog;
}

class GameOverDialog : public QDialog
{
    Q_OBJECT

public:
    explicit GameOverDialog(QWidget *parent = nullptr);
    void setOutcome(bool);
    ~GameOverDialog();

signals:
    void newGame();
    void similarGame();
private:
    Ui::GameOverDialog *ui;
};

#endif // GAMEOVERDIALOG_H
