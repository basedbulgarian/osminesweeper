#include "gameoverdialog.h"
#include "ui_gameoverdialog.h"

GameOverDialog::GameOverDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GameOverDialog)
{
    ui->setupUi(this);
    connect(ui->new_game,SIGNAL(clicked()),SIGNAL(newGame()));
    connect(ui->similar_game,SIGNAL(clicked()),SIGNAL(newGame()));
    connect(ui->decide_later,SIGNAL(clicked()),SLOT(hide()));
}
void GameOverDialog::setOutcome(bool result)
{
    if (result)
    {
        ui->outcome->setText("You won! 😁 Play again?");
    }
    else{
        ui->outcome->setText("You lost. 🤭 Try again?");
    }
}
GameOverDialog::~GameOverDialog()
{
    delete ui;
}
