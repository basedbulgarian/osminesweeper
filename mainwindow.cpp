#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QBrush>
#include <QGraphicsRectItem>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    loadFonts();

    ui->setupUi(this);
    scene = new QGraphicsScene();
    ui->graphicsView->setScene(scene);

    num_mines = 10;
    mine_marks = 0;
    timer_count = 0;

    connect(this,SIGNAL(game_over(QString)),SLOT(handleGameOver(QString)));
    connect(this,SIGNAL(changeMineMarksDisplay(int)),this,SLOT(showMineMarks(int)));

    ui->mines->setText(QString::number(num_mines));
    ui->time->setText(QString::number(timer_count));

    connect(&dialog,SIGNAL(newGame()),SLOT(startSimilarGame()));
    connect(&dialog,SIGNAL(similarGame()),SLOT(startSimilarGame()));

    timer.setInterval(1000);
    connect(&timer,SIGNAL(timeout()),this,SLOT(updateTimerDisplay()));

    for (int i = 0; i < 9; i++)
    {
        QVector<MineRect*> row;
        for (int j = 0; j < 9; j++)
        {
            MineRect* rect = new MineRect(static_cast<double>(70*i),static_cast<double>(70*j),this);
            scene->addItem(rect);
            row.append(rect);
            connect(rect,SIGNAL(redrawRect(QRectF,QPair<int,int>)),scene,SLOT(update(const QRectF&)));
            connect(rect,SIGNAL(stateChanged(QRectF,QPair<int,int>)),SLOT(tileFlipped(QRectF,QPair<int,int>)));
            connect(rect,SIGNAL(stateChanged(QRectF,QPair<int,int>)),SLOT(countMineMarks(QRectF,QPair<int,int>)));
            connect(rect,SIGNAL(massFlip(QPair<int,int>)),SLOT(handleMassFlip(QPair<int,int>)));
            connect(rect,SIGNAL(numClicked(QPair<int,int>)),SLOT(openAdjacent(QPair<int,int>)));
            connect(rect,SIGNAL(game_over(QString)),SIGNAL(game_over(QString)));
            rect->setLocation(QPair<int,int>(i,j));
        }
        rects.append(row);
    }

    fill_mines();

    calc_numbers();
}
void MainWindow::zero_all_rects()
{
    for (int i = 0; i < rects.length(); i++)
    {
        for (int j = 0; j < rects[i].length(); j++)
        {
            rects[i][j]->setState(MineRect::DEFAULT);
            rects[i][j]->setNum(0);
        }
    }
    mines.clear();
    nonmines.clear();
    mine_marks = 0;
    emit changeMineMarksDisplay(num_mines);
    timer.stop();
    timer_count = 0;
    ui->time->setText(QString::number(timer_count));
}
void MainWindow::fill_mines()
{
    QRandomGenerator* generator = QRandomGenerator::system();
    for (int i = 0; i < num_mines; i++)
    {
        while (true)
        {
            int x = generator->bounded(static_cast<int>(0),static_cast<int>(9));
            int y = generator->bounded(static_cast<int>(0),static_cast<int>(9));
            if (mines.contains(QPair<int,int>(x,y)))
            {
                continue;
            }
            bool found_empty_rect = false;
            for (int j = x-1; j <= x+1; j++)
            {
                if (j < 0 || j > 8)
                {
                    continue;
                }
                for (int k = y-1; k <= y+1; k++)
                {
                    if (k < 0 || k > 8)
                    {
                        continue;
                    }
                    if (rects[j][k]->getNum() != -1)
                    {
                        found_empty_rect = true;
                        break;
                    }
                }
                if (found_empty_rect)
                {
                    break;
                }
            }
            if (!found_empty_rect)
            {
                continue;
            }
            mines += QPair<int,int>(x,y);
            rects[x][y]->setNum(static_cast<qint32>(-1));
            break;
        }
    }
}
void MainWindow::calc_numbers()
{
    for (QSet<QPair<int,int>>::iterator i = mines.begin(); i != mines.end(); i++)
    {
        for (int k = i->first-1; k <= i->first+1; k++)
        {
            if (k < 0 || k > 8)
            {
                continue;
            }
            for (int l = i->second-1; l <= i->second+1; l++)
            {
                if (l < 0 || l > 8)
                {
                    continue;
                }
                if (rects[k][l]->getNum() != -1)
                {
                    rects[k][l]->setNum(rects[k][l]->getNum() + 1);
                    nonmines += QPair<int,int>(k,l);
                }
            }
        }
    }
}
void MainWindow::handleMassFlip(QPair<int,int> location)
{
    for (int i = location.first-1; i <= location.first+1; i++)
    {
        if (i < 0 || i > 8)
        {
            continue;
        }
        for (int j = location.second-1; j <= location.second+1; j++)
        {
            if (j < 0 || j > 8 || (i == location.first && j == location.second))
            {
                continue;
            }
            if (rects[i][j]->getNum() == 0 && rects[i][j]->getState() == MineRect::DEFAULT)
            {
                rects[i][j]->setState(MineRect::FLIPPED);
                handleMassFlip(QPair<int,int>(i,j));
            }
            else if (rects[i][j]->getState() == MineRect::DEFAULT)
            {
                rects[i][j]->setState(MineRect::FLIPPED);
            }
        }
    }
}
void MainWindow::openAdjacent(QPair<int,int> location)
{
    int num_flags = 0;
    for (int i = location.first-1; i <= location.first+1; i++)
    {
        if (i < 0 || i > 8)
        {
            continue;
        }
        for (int j = location.second-1; j <= location.second+1; j++)
        {
            if (j < 0 || j > 8 || (i == location.first && j == location.second))
            {
                continue;
            }
            if (rects[i][j]->getState() == MineRect::MARKED)
            {
                num_flags++;
            }
        }
    }
    if (num_flags == rects[location.first][location.second]->getNum())
    {
        for (int i = location.first-1; i <= location.first+1; i++)
        {
            if (i < 0 || i > 8)
            {
                continue;
            }
            for (int j = location.second-1; j <= location.second+1; j++)
            {
                if (j < 0 || j > 8 || (i == location.first && j == location.second))
                {
                    continue;
                }
                if (rects[i][j]->getState() == MineRect::DEFAULT)
                {
                    rects[i][j]->setState(MineRect::FLIPPED);
                    if (rects[i][j]->getNum() == 0)
                    {
                        handleMassFlip(QPair<int,int>(i,j));
                    }
                    else if (rects[i][j]->getNum() == -1)
                    {
                        emit game_over("lost");
                        return;
                    }
                }
            }
        }
    }
}
void MainWindow::handleGameOver(QString result)
{
    timer.stop();
    setProperty("game_over",true);
    if (result == "lost")
    {
        for (QSet<QPair<int,int>>::iterator i = mines.begin(); i != mines.end(); i++)
        {
            rects[i->first][i->second]->setState(MineRect::FLIPPED);
        }
        dialog.setOutcome(false);
        dialog.show();
    }
    else{
        for (QSet<QPair<int,int>>::iterator i = mines.begin(); i != mines.end(); i++)
        {
            rects[i->first][i->second]->setState(MineRect::MARKED);
        }
        dialog.setOutcome(true);
        dialog.show();
    }
}
void MainWindow::tileFlipped(QRectF, QPair<int,int> location)
{
    if (!timer.isActive() && rects[location.first][location.second]->getNum() != -1)
    {
        timer.start();
    }
    MineRect* current_rect = rects[location.first][location.second];
    if ((current_rect->getState() == MineRect::FLIPPED || current_rect->getState() == MineRect::FLIPPING) && current_rect->getNum() >= 0)
    {
        nonmines -= location;
        if (nonmines.size() == 0)
        {
            emit game_over("won");
        }
    }
}
void MainWindow::countMineMarks(QRectF, QPair<int,int> location)
{
    if (!property("game_over").toBool())
    {
        if (rects[location.first][location.second]->getState() == MineRect::DEFAULT)
        {
            emit changeMineMarksDisplay(num_mines-(--mine_marks));
        }
        else if (rects[location.first][location.second]->getState() == MineRect::MARKED)
        {
            emit changeMineMarksDisplay(num_mines - (++mine_marks));
        }
    }
    else{
        emit changeMineMarksDisplay(0);
    }
}
void MainWindow::updateTimerDisplay()
{
    ui->time->setText(QString::number(++timer_count));
}
void MainWindow::startSimilarGame()
{
    if (dialog.isVisible())
    {
        dialog.hide();
    }
    setProperty("game_over",false);
    zero_all_rects();
    fill_mines();
    calc_numbers();
}
void MainWindow::showMineMarks(int num_marks)
{
    ui->mines->setText(QString::number(num_marks));
}
void MainWindow::loadFonts()
{
    QFontDatabase::addApplicationFont(":/fonts/Segment7Standard.otf");
}
MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_reset_clicked()
{
    startSimilarGame();
}
