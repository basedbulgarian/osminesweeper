#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QDebug>
#include <QRandomGenerator>
#include <QTimer>
#include <QFontDatabase>
#include "gameoverdialog.h"
#include "minerect.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    void fill_mines();
    void zero_all_rects();
    void calc_numbers();
    void loadFonts();
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    static bool lost_state;
public slots:
    void handleMassFlip(QPair<int,int>);
    void openAdjacent(QPair<int,int>);
    void handleGameOver(QString);
    void tileFlipped(QRectF,QPair<int,int>);
    void countMineMarks(QRectF,QPair<int,int>);
    void startSimilarGame();
    void showMineMarks(int);
private slots:

    void on_reset_clicked();
    void updateTimerDisplay();
signals:
    void game_over(QString);
    void changeMineMarksDisplay(int);
private:
    int num_mines;
    Ui::MainWindow *ui;
    QGraphicsScene* scene;
    QVector<QVector<MineRect*>> rects;
    double zoom_factor;
    QSet<QPair<int,int>> mines;
    QSet<QPair<int,int>> nonmines;
    int mine_marks;
    QTimer timer;
    quint64 timer_count;
    GameOverDialog dialog;
};
#endif // MAINWINDOW_H
