﻿#include "minerect.h"

const QImage MineRect::mine_img = MineRect::loadImage(":/imgs/mine.png");
const QImage MineRect::flag_img = MineRect::loadImage(":/imgs/flag.png");

MineRect::MineRect()
{
    num = 0;
    state = DEFAULT;
    bounding_rect = new QRectF(pos().x(),pos().y(),DEFAULT_SIZE,DEFAULT_SIZE);
    connect(this,SIGNAL(stateChanged(QRectF,QPair<int,int>)),this,SIGNAL(redrawRect(QRectF,QPair<int,int>)));
    connect(this,SIGNAL(backgroundColorChanged(QColor)),SLOT(redrawForBackgroundColorChange()));
    m_backgroundColor = DEFAULT_BACKGROUND_COLOR;
}
MineRect::MineRect(qint32 num, RECT_STATE state, QObject* parent = nullptr)
{
    this->num = num;
    this->state = state;
    this->setParent(parent);
    connect(this,SIGNAL(stateChanged(QRectF,QPair<int,int>)),this,SIGNAL(redrawRect(QRectF,QPair<int,int>)));
    connect(this,SIGNAL(backgroundColorChanged(QColor)),SLOT(redrawForBackgroundColorChange()));
    m_backgroundColor = DEFAULT_BACKGROUND_COLOR;
}
MineRect::MineRect(double x,double y,qint32 num,RECT_STATE state, QObject* parent = nullptr)
{
    setPos(x,y);
    this->num = num;
    this->state = state;
    this->setParent(parent);
    connect(this,SIGNAL(stateChanged(QRectF,QPair<int,int>)),this,SIGNAL(redrawRect(QRectF,QPair<int,int>)));
    connect(this,SIGNAL(backgroundColorChanged(QColor)),SLOT(redrawForBackgroundColorChange()));
    m_backgroundColor = DEFAULT_BACKGROUND_COLOR;
}
MineRect::MineRect(double x, double y, QObject* parent = nullptr)
{
    bounding_rect = new QRectF(x,y,DEFAULT_SIZE,DEFAULT_SIZE);
    num = 0;
    state = DEFAULT;
    this->setParent(parent);
    connect(this,SIGNAL(stateChanged(QRectF,QPair<int,int>)),this,SIGNAL(redrawRect(QRectF,QPair<int,int>)));
    connect(this,SIGNAL(backgroundColorChanged(QColor)),SLOT(redrawForBackgroundColorChange()));
    m_backgroundColor = DEFAULT_BACKGROUND_COLOR;
}
void MineRect::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
{
    if (state == DEFAULT)
    {
        QBrush green_brush(DEFAULT_BACKGROUND_COLOR);
        QPen green_pen(DEFAULT_BACKGROUND_COLOR);
        painter->setPen(green_pen);
        painter->setBrush(green_brush);
        painter->drawRoundedRect(boundingRect(),8,8);
    }
    else if (state == FLIPPED)
    {
        QBrush light_green_brush(QColor(185,246,202));
        QPen light_green_pen(QColor(185,246,202));
        painter->setPen(light_green_pen);
        painter->setBrush(light_green_brush);
        painter->drawRoundedRect(boundingRect(),8,8);
        if (num != -1 && num != 0)
        {
            QPen black_pen(Qt::black);
            painter->setPen(black_pen);
            painter->setFont(QFont("Arial",20));
            painter->drawText(boundingRect(),Qt::AlignCenter | Qt::AlignVCenter,QString::number(num));
        }
        else if (num == -1)
        {
            painter->drawImage(boundingRect(),mine_img,QRectF(0,0,512,512));
        }
    }
    else if (state == MARKED)
    {
        QBrush green_brush(QColor(0,200,83));
        QPen green_pen(QColor(0,200,83));
        painter->setPen(green_pen);
        painter->setBrush(green_brush);
        painter->drawRoundedRect(boundingRect(),8,8);
        QRectF dest_rect = boundingRect();
        dest_rect.setX(dest_rect.x()+8);
        dest_rect.setY(dest_rect.y()+8);
        dest_rect.setWidth(dest_rect.width()-8);
        dest_rect.setHeight(dest_rect.height()-8);
        painter->drawImage(dest_rect,flag_img,QRectF(0,0,512,512));
    }
    else if (state == FLIPPING)
    {
        QBrush green_brush(m_backgroundColor);
        QPen green_pen(m_backgroundColor);
        painter->setPen(green_pen);
        painter->setBrush(green_brush);
        painter->drawRoundedRect(boundingRect(),8,8);
    }
}
qint32 MineRect::getNum()
{
    return num;
}
void MineRect::setNum(qint32 num)
{
    this->num = num;
}
MineRect::RECT_STATE MineRect::getState()
{
    return state;
}
void MineRect::setState(RECT_STATE state)
{
    if (state == DEFAULT)
    {
        m_backgroundColor = DEFAULT_BACKGROUND_COLOR;
    }
    else if (state == FLIPPED && !parent()->property("game_over").toBool() && num != -1)
    {
        this->state = FLIPPING;
        emit stateChanged(boundingRect(),location);
        animateFlip(QColor(0,200,83),QColor(185,246,202));
        return;
    }
    this->state = state;
    emit stateChanged(boundingRect(),location);

    if (state == FLIPPED && !parent()->property("game_over").toBool() && num == -1)
    {
        emit game_over("lost");
    }
}
QRectF MineRect::boundingRect() const
{
    return *bounding_rect;
}
void MineRect::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (!parent()->property("game_over").toBool())
    {
        if (state == DEFAULT)
        {
            if (event->button() == Qt::LeftButton)
            {
                setState(FLIPPED);
            }
            if (event->button() == Qt::RightButton)
            {
                setState(MARKED);
            }
        }
        else if (state == MARKED)
        {
            if (event->button() == Qt::RightButton)
            {
                setState(DEFAULT);
            }
        }
        else if (state == FLIPPED)
        {
            if (num != 0)
            {
                emit numClicked(location);
            }
        }
    }
}
void MineRect::setLocation(QPair<int, int> loc)
{
    location = loc;
}
QImage MineRect::loadImage(QString path)
{
    Q_INIT_RESOURCE(resources);
    QImage image(path);
    return image;
}
void MineRect::animateFlip(QColor start_color, QColor end_color)
{
    QPropertyAnimation* anim = new QPropertyAnimation(this, "backgroundColor");
    anim->setDuration(150);
    anim->setStartValue(start_color);
    anim->setEndValue(end_color);
    anim->setEasingCurve(QEasingCurve::OutCubic);

    connect(anim,SIGNAL(finished()),this,SLOT(finishFlipping()));

    anim->start(QAbstractAnimation::DeleteWhenStopped);
}
void MineRect::finishFlipping()
{
    state = FLIPPED;
    emit stateChanged(boundingRect(),location);
    if (num == 0)
    {
        emit massFlip(location);
    }
}
void MineRect::redrawForBackgroundColorChange()
{
    emit redrawRect(boundingRect(),location);
}
