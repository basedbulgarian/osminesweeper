#ifndef MINERECT_H
#define MINERECT_H

#include <QObject>
#include <QGraphicsObject>
#include <QPainter>
#include <QDebug>
#include <QImage>
#include <QGraphicsSceneMouseEvent>
#include <QDir>
#include <QPropertyAnimation>

class MineRect : public QGraphicsObject
{
    Q_OBJECT
    Q_PROPERTY(QColor backgroundColor MEMBER m_backgroundColor NOTIFY backgroundColorChanged)
    qint32 num;
    QRectF* bounding_rect;
    const int DEFAULT_SIZE = 60;
    static const QImage mine_img;
    static const QImage flag_img;
    QPair<int,int> location;
    static QImage loadImage(QString);
    const QColor DEFAULT_BACKGROUND_COLOR = QColor(0,200,83);
    QColor m_backgroundColor;
    void animateFlip(QColor,QColor);
signals:
    void stateChanged(QRectF,QPair<int,int>);
    void massFlip(QPair<int,int>);
    void numClicked(QPair<int,int>);
    void game_over(QString);
    void redrawRect(QRectF,QPair<int,int>);
    void backgroundColorChanged(QColor);
public:
    enum RECT_STATE {DEFAULT,MARKED,FLIPPED,FLIPPING};
    MineRect();
    MineRect(qint32,RECT_STATE,QObject*);
    MineRect(double,double,QObject*);
    MineRect(double,double,qint32,RECT_STATE,QObject*);
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    qint32 getNum();
    void setNum(qint32);
    RECT_STATE getState();
    void setState(RECT_STATE);
    void mousePressEvent(QGraphicsSceneMouseEvent*);
    void setLocation(QPair<int,int>);
private slots:
    void finishFlipping();
    void redrawForBackgroundColorChange();
private:
    RECT_STATE state;
};
#endif // MINERECT_H
